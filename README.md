# cluster-prometheus

## Steps (using prometheus operator release 0.49.0)
### Install prometheus-operator
1. Modify the subject on ClusterRoleBindings.
subjects: \
&nbsp;&nbsp;\- kind: ServiceAccount \
&nbsp;&nbsp;&nbsp;&nbsp;name: prometheus-operator \
&nbsp;&nbsp;&nbsp;&nbsp;**namespace: monitoring**
1. kubectl apply -n monitoring -f prometheus-operator/
### Install prometheus instance
1. kubectl apply -n monitoring -f prometheus/
### Install service monitors of kubernetes cluster components (kubelet,apiserver,dns,etc)
1. kubectl apply -n monitoring -f cluster-components/
### Install service monitor for node-exporter containers to pull kubernetes nodes metrics
1. kubectl apply -n monitoring -f node-exporter/
### Install service monitor to pull pods resource usage
1. kubectl apply -n monitoring -f kube-state-metrics/

#### Changed images to support multiarch.
quay.io/coreos/kube-rbac-proxy:v0.4.1    -> quay.io/brancz/kube-rbac-proxy:v0.10.0 \
quay.io/coreos/kube-state-metrics:v1.9.5 -> k8s.gcr.io/kube-state-metrics/kube-state-metrics:v2.1.0

#### If any changes are made:
k delete -n monitoring pod/prometheus-prometheus-0

<br><br>
## Links
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/user-guides/getting-started.md
<br><br>
https://github.com/prometheus-operator/prometheus-operator
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/rbac.md
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/user-guides/alerting.md
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/design.md
