# service monitoring

Components:
1. ClusterRole: will define resources I have permissions to scrape from.
```sh
  - nodes
  - nodes/metrics
  - services
  - endpoints
  - pods
```
2. RoleBinding & ServiceAccount: just the union between a user account and the role.
3. Prometheus: a prometheus object (CRD) that joins a service account with sufficient permissions and a label to match ServiceMonitor objects.
4. Service: a NodePort service to access the prometheus instance.
5. ServiceMonitor: finally, this will define which service you will scrape based on the labels of the service of your applications. 

<br><br>
## Links
https://github.com/prometheus-operator/prometheus-operator/blob/master/Documentation/user-guides/getting-started.md
https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/monitoring/prometheus/kubernetes/1.18.4/prometheus-cluster-monitoring
##### For labels documentation
https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/